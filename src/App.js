import React from 'react';
import logo from './logo.svg';
import './App.css';
import LeftSide from './components/LeftSide'
import RightSide from './components/RightSide'
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Switch, Route } from "react-router-dom"

function App() {
  return (
    <div>
      {/* <LeftSide/> */}
      <BrowserRouter>
      <Switch>
      <Route path="/right_side" component={RightSide} />
      <Route component={LeftSide} />
      </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
