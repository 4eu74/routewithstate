import React, {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import { BrowserRouter, Switch, Route, Link } from "react-router-dom"
// import RightSide from './RightSide'

export default function LeftSide() {
    console.log('rendered')
    const [state, setState] = useState(true)
    const useForceUpdate = () => useState()[1];
    const forceUpdate = useForceUpdate();
    const setNew = () => {
        setState(!state)
        console.log(state)
    }
    return (
        <div>
            <Container>
                <Row>
                    <Col><Link to="/right_side">Show Right Side Page</Link></Col>
                    <Col> 
                    
                    </Col>
                    
                </Row>
            </Container>
        </div>
    )
}

function RightSide(state) {
    const [isShow, setIsShow] = useState(state.stateData)
    // function stateChange(data) {
    //     setIsShow(data)
    // }
    // stateChange(stateData)

    return (
        <div>
        {
            isShow ? (<RightSideBody/>) : (<RightSideBodyNull/>)
        }
        </div>
    )
}

function RightSideBody() {
    return (
        <div>
            <p>This is right side</p>
        </div>
    )
}

export function RightSideBodyNull() {
    return (
        <div>
            
        </div>
    )
}