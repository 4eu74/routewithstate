import React, { useState } from 'react'
import LeftSide from './LeftSide'
import { Container, Row, Col } from 'react-bootstrap';
export default function RightSide() {
    // const [isShow, setIsShow] = useState(state.stateData)
    // function stateChange(data) {
    //     setIsShow(data)
    // }
    // stateChange(stateData)

    return (
        <div>
        {/* {
            isShow ? (<RightSideBody/>) : (<RightSideBodyNull/>)
        } */}
        <Container>
                <Row>
        <Col><LeftSide/></Col>
        <Col><RightSideBody/></Col>
        </Row></Container>
        </div>
    )
}

function RightSideBody() {
    return (
        <div>
            <p>This is right side</p>
        </div>
    )
}

export function RightSideBodyNull() {
    return (
        <div>
            
        </div>
    )
}

